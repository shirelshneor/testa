<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'a',
                    'email' => 'a@a.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'manager' ,
                ],
                [
                    'name' => 'b',
                    'email' => 'b@b.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'salesrep' ,
                ],
                [
                    'name' => 'c',
                    'email' => 'c@c.com',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role' => 'salesrep' ,
                ],
               
            ]);
    }


}
